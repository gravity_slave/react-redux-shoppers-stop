import {ADD_POCKET_MONEY, REMOVE_POCKET_MONEY } from '../actions';
import addTo from './helper'

export default function pocketMoney(state = 50, action) {
  switch (action.type) {
    case ADD_POCKET_MONEY:
      let item = addTo(action.id);
      let pocketMoney = state + item.cost;
      return pocketMoney;
    case REMOVE_POCKET_MONEY:
       item = addTo(action.id);
       pocketMoney = state - item.cost;
      return pocketMoney;
    default:
      return state;
  }
}