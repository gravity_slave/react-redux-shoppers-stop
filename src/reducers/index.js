import grocery from './grocey_reducer';
import  { combineReducers } from 'redux';
import shoppingbag from './shoppingbag_reducer'
import pocketMoney from './pocket_reducer';
const rootReducer = combineReducers({
  grocery,
  shoppingbag,
  pocketMoney
})

export default rootReducer;