import { ADD_GROCERY, REMOVE_GROCERY  } from '../actions';
import addTo from './helper'

export default function shoppingbag(state = [], action) {
  switch (action.type) {
    case ADD_GROCERY:
      let shoppingBag = [...state, addTo(action.id)];
      return shoppingBag;
    case REMOVE_GROCERY:
      shoppingBag = state.filter( bag => bag.id !== action.id);
      return shoppingBag;
    default:
      return state;
  }
}


