import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addGroceryById, removePocketMoney } from '../actions';

class Grocery extends Component {
  render() {
    return (
      <div className="col-md-4 grocery-bg">
        <h2 className='text-center'>Grocery Items</h2>
        <ul className="list-group">
          {this.props.grocery.map((g, i) => (
            <li
              onClick={ e => {
                this.props.addGroceryById(g.id);
                this.props.removePocketMoney(g.id)
              }}
              className="list-group-item"
              key={i}
            >
              <b>{g.name}</b> - <span className="label label-info">{g.cost} $</span> - <span className="label label-primary">{g.calories} kcal</span> -<span className="label label-warning">{g.weight} mg</span>
            </li>
          ))}

        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  console.log(state)
  return {
    grocery: state.grocery
  };
}

export default connect(mapStateToProps, {addGroceryById, removePocketMoney})(Grocery);