import React, { Component } from 'react';
import { connect } from 'react-redux';

import { removeGroceryById, addPocketMoney } from '../actions';

class ShoppingBag extends Component {

  displayBag() {
    if (this.props.shoppingbag.length > 0) {
     return (<ul className="list-group">
        {this.props.shoppingbag.map( (bag, index) => (
          <li
            className="list-group-item"
            key={index}
            onClick={ () => {
              this.props.removeGroceryById(bag.id);
              this.props.addPocketMoney(bag.id);
            }}
          >
            <b>{bag.name}</b> - <span className="label label-info">{bag.cost} $</span> - <span className="label label-primary">{bag.calories} kcal</span> -<span className="label label-warning">{bag.weight} mg</span>
          </li>
        ))}
      </ul>)
    } else {
      return (<ul className="list-group">

       <li className="list-group-item">No items in the bag!</li>
      </ul>)
    }
  }
  render() {
    return (
      <div className="col-md-4 shopping-bag-bg">
        <h2 className='text-center'>Shopping Bag</h2>
        {this.displayBag()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    shoppingbag: state.shoppingbag
  }
}

export default connect(mapStateToProps, {removeGroceryById, addPocketMoney})(ShoppingBag);