import React, { Component } from 'react';
import { connect } from 'react-redux';

class Stats extends Component {
  cost () {
    let totalCost = 0;
    this.props.shoppingbag.forEach( bag => totalCost += bag.cost );
    return totalCost;
  }
  weight () {
    let totalWeight = 0;
    this.props.shoppingbag.forEach( bag => totalWeight += bag.weight );
    return totalWeight;
  }

  cal () {
    let totalCal = 0;
    this.props.shoppingbag.forEach( bag => totalCal += bag.calories );
    return totalCal;
  }
  render() {

    return (
      <div className="col-md-3 stats-bg">
        <h2 className='text-center'>Stats</h2>

        <ul className="list-group">
          <li className="list-group-item">cost: {this.cost()}$</li>

          <li className="list-group-item">calories: {this.cal()} kcal</li>
          <li className="list-group-item">weight: {this.weight()} gr</li>
        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    shoppingbag: state.shoppingbag
  }
}

export default connect(mapStateToProps, null)(Stats);