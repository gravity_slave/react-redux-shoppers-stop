import React, { Component } from 'react';
import { connect } from 'react-redux';

class Pocket extends Component {
  render() {
    return (
      <div className="row">
        <h2 className='text-center'>You have <span className="label label-success">
          {this.props.pocketMoney}$
        </span> amount to spend on Grocery</h2>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    pocketMoney: state.pocketMoney
  }
}
export default connect(mapStateToProps, null)(Pocket);