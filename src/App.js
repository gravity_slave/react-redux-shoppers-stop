import React, { Component } from 'react';
import Grocery from './components/Grocery';
import ShoppingBag from './components/ShoppingBag';
import Stats from './components/Stats';
import Pocket from  './components/Pocket';

class App extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='jumbotron text-center'>
            <h1>Shoppers Stop!</h1>
            <p>Where you cant get your daily meal</p>
          </div>
        </div>
        <Pocket />
        <br/>
        <div className='row'>
          <Grocery/>
          <ShoppingBag/>
          <Stats/>
        </div>

      </div>
    );
  }
}

export default App;
