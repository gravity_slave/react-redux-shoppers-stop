import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './App';
import groceryReducer from './reducers';

const store = createStore(groceryReducer);

ReactDOM.render(
  <Provider  store={store}>
  <App/>
</Provider>, document.getElementById('root'));
registerServiceWorker();
